import React, { useState } from 'react';
import useData from '../../hook/getData';
import PokemonCard from './Components/PokemonCard';

import s from './Pokedex.module.scss';
import { IPokemons, PokemonsRequest } from '../../interface/pokemons';
import useDebounce from '../../hook/useDebounce';

interface IQuery {
  name?: string;
}

const Pokedex = () => {
  const [searchValue, setSearchValue] = useState('');
  const [query, setQuery] = useState<IQuery>({});

  const debounceValue = useDebounce(searchValue, 500);

  const { data, isLoading, isHasError } = useData<IPokemons>('getPokemons', query, [debounceValue]);

  if (isHasError) {
    return <div>Something was wrong</div>;
  }

  const handleSearchInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchValue(e.target.value);
    setQuery((state: IQuery) => ({
      ...state,
      name: e.target.value,
    }));
  };

  return (
    <div>
      <div className={s.root}>
        <h1>{!isLoading && data && data.total} - we have pokemons!</h1>
        <input type="text" value={searchValue} onChange={handleSearchInput} />
        <div className={s.inner}>
          {!isLoading &&
            data &&
            data.pokemons.map((pokemon: PokemonsRequest) => <PokemonCard key={pokemon.name} {...pokemon} />)}
        </div>
      </div>
    </div>
  );
};

export default Pokedex;
