import React from 'react';

interface EmptyPageProps {
  title?: string;
}

const EmptyPage: React.FC<EmptyPageProps> = ({ title }) => (
  <div>This is EmptyPage {title && <span>fro {title}</span>}</div>
);

export default EmptyPage;
