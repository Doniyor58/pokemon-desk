import React from 'react';
import { navigate } from 'hookrouter';

import Button from '../../components/Button';
import Heading from '../../components/Heading';
import Footer from '../../components/Footer';
import Parallax from './components/Parallax';

import { LinksEnum } from '../../routes';

import s from './Home.module.scss';

const Home = () => (
  <div className={s.root}>
    <div className={s.content}>
      <div className={s.inner}>
        <div className={s.text}>
          <div className={s.title}>
            <Heading size="h1" lineBreak>
              <b>Find</b>
              {` all your\nfavorite\n`}
              <b>Pokemon</b>
            </Heading>
          </div>
          <div className={s.description}>
            <Heading size="h2" lineBreak>
              {`You can know the type of Pokemon,\nits strengths, disadvantages and\nabilities`}
            </Heading>
          </div>
          <Button size="sm" onClick={() => navigate(LinksEnum.POKEDEX)}>
            See pokemons
          </Button>
        </div>

        <div className={s.parallax}>
          <Parallax />
        </div>
      </div>
    </div>
    <Footer />
  </div>
);

export default Home;
