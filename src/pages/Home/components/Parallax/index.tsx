import React, { useState, useEffect } from 'react';
import s from './Parallax.module.scss';

import CloudSmallPNG from './assets/cloudSmall.png';
import CloudBigPNG from './assets/cloudBig.png';
import PickachuPNG from './assets/pikachu.png';
import PokebollSmallPNG from './assets/pokeballSmall.png';
import PokebollBigPNG from './assets/pokeballBig.png';

const Parallax = () => {
  const [screenX, setScreenX] = useState(0);
  const [screenY, setScreenY] = useState(0);

  useEffect(() => {
    const handleMouse = (event: MouseEvent) => {
      setScreenX(event.screenX);
      setScreenY(event.screenY);
    };

    document.addEventListener('mousemove', handleMouse);

    return () => document.removeEventListener('mousemove', handleMouse);
  }, [screenX, screenY]);

  return (
    <>
      <div className={s.root}>
        <div className={s.cloudSmall}>
          <img
            src={CloudSmallPNG}
            alt="Cloud Small"
            style={{
              transform: `translate(-${screenX * 0.005}px, -${screenY * 0.005}px)`,
            }}
          />
        </div>

        <div className={s.cloudBig}>
          <img
            src={CloudBigPNG}
            alt="Cloud Big"
            style={{
              transform: `translate(-${screenX * 0.01}px, -${screenY * 0.01}px)`,
            }}
          />
        </div>

        <div className={s.pickachu}>
          <img
            src={PickachuPNG}
            alt="Pickachu"
            style={{
              transform: `translate(${screenX * 0.005}px, ${screenY * 0.005}px)`,
            }}
          />
        </div>

        <div className={s.pokeballSmall}>
          <img
            src={PokebollSmallPNG}
            alt="Pokeboll Small"
            style={{
              transform: `translate(${screenX * 0.01}px, ${screenY * 0.01}px)`,
            }}
          />
        </div>
      </div>

      <div className={s.pokeballBig}>
        <img
          src={PokebollBigPNG}
          alt="Pokeboll Big"
          style={{
            transform: `translate(-${screenX * 0.01}px, -${screenY * 0.01}px)`,
          }}
        />
      </div>
    </>
  );
};

export default Parallax;
