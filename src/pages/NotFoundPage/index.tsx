import React from 'react';
import { navigate } from 'hookrouter';

import Heading from '../../components/Heading';
import Button from '../../components/Button';
import { LinksEnum } from '../../routes';

import RocketTrioPNG from './assets/trio.png';
import s from './NotFoundPage.module.scss';

const NotFoundPage: React.FC = (props) => (
  <div className={s.root}>
    <div className={s.inner}>
      <div className={s.errorCode}>404</div>

      <div className={s.content}>
        <div className={s.img}>
          <img src={RocketTrioPNG} alt="Rocket Trio " />
        </div>

        <div className={s.text}>
          <Heading size="h2">
            <span className={s.textWhite}>The rocket team </span>
            <span>has won this time.</span>
          </Heading>
        </div>

        <Button yellow size="sm" onClick={() => navigate(LinksEnum.HOME)}>
          Return
        </Button>
      </div>
    </div>
  </div>
);

export default NotFoundPage;
