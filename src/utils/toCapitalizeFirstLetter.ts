const toCapitalizeFirstLetter = (str: string): string => {
  if (!str) return str;

  const capitalizedFirstLetter = str[0].toUpperCase();
  const remainingWord = str.slice(1).toLowerCase();

  return `${capitalizedFirstLetter}${remainingWord}`;
};

export default toCapitalizeFirstLetter;
