import toCapitalizeFirstLetter from '../toCapitalizeFirstLetter';

describe('Функция toCapitalizeFirstLetter, изменяющая первый символ на заглавный, а остальные символы приводящая в нижний регистр', () => {
  test('должна из слова "pokemonApp" вернуть "Pokemonapp"', () => {
    expect(toCapitalizeFirstLetter('pokemonApp')).toEqual('Pokemonapp');
  });

  test('должна из слова "jestLesson" вернуть "Jestlesson"', () => {
    expect(toCapitalizeFirstLetter('jestLesson')).toEqual('Jestlesson');
  });

  test('должна из слова "zARMARATHON" вернуть "Zarmarathon"', () => {
    expect(toCapitalizeFirstLetter('zARMARATHON')).toEqual('Zarmarathon');
  });

  test('должна при пустой строке так же вернуть пустую строку', () => {
    expect(toCapitalizeFirstLetter('')).toEqual('');
  });
});
