import getUrlWithParamsConfig from '../getUrlWithParamsConfig';

describe('getUrlWithParamsConfig', () => {
  test('должна принимать 2 аргумента "getPokemons" и пустой объект, на выходе возвращать с полями pathname, protocol, host, query', () => {
    const url = getUrlWithParamsConfig('getPokemons', {});

    expect(url).toEqual({
      protocol: 'http',
      host: 'zar.hosthot.ru',
      pathname: '/api/v1/pokemons',
      query: {},
    });
  });

  test('должна принимать 2 аргумента "getPokemons" и { name: "Pikachu" }, на выходе возвращать с полями pathname, protocol, host и query с полем name равным "Pikachu"', () => {
    const url = getUrlWithParamsConfig('getPokemons', { name: 'Pikachu' });

    expect(url).toEqual({
      protocol: 'http',
      host: 'zar.hosthot.ru',
      pathname: '/api/v1/pokemons',
      query: { name: 'Pikachu' },
    });
  });

  test('должна принимать 2 аргумента "getPokemon" и { id: 25 }, на выходе возвращать с полями pathname, protocol, host и пустым query ', () => {
    const url = getUrlWithParamsConfig('getPokemon', { id: 25 });

    expect(url).toEqual({
      protocol: 'http',
      host: 'zar.hosthot.ru',
      pathname: '/api/v1/pokemon/25',
      query: {},
    });
  });
});
