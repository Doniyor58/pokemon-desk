import Url from 'url';
import getUrlWithParamsConfig from './getUrlWithParamsConfig';

async function req<T>(entrypoint: string, query: object): Promise<T> {
  const uri = Url.format(getUrlWithParamsConfig(entrypoint, query));

  const result = await fetch(uri).then((data) => data.json());
  return result;
}

export default req;
