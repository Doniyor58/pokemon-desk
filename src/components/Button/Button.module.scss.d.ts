// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'lg': string;
  'md': string;
  'root': string;
  'sm': string;
  'yellow': string;
}
export const cssExports: CssExports;
export default cssExports;
