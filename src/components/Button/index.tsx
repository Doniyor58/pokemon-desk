import React from 'react';
import cn from 'classnames';
import s from './Button.module.scss';

interface ButtonProps {
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
  size: 'sm' | 'md' | 'lg';
  yellow?: boolean;
}

const Button: React.FC<ButtonProps> = ({ children, onClick, size, yellow }) => (
  <button type="button" className={cn(s.root, s[size], yellow && s.yellow)} onClick={onClick}>
    {children}
  </button>
);

export default Button;
