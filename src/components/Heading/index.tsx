import React from 'react';
import cn from 'classnames';
import s from './Heading.module.scss';

interface HeadingProps {
  size: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
  inline?: boolean;
  lineBreak?: boolean;
}

const Heading: React.FC<HeadingProps> = ({ children, size, inline, lineBreak }) => (
  <p className={cn(s[size], inline && s.inline, lineBreak && s.lineBreak)}>{children}</p>
);

export default Heading;
