import React from 'react';
import s from './Footer.module.scss';

const Footer = () => (
  <footer className={s.root}>
    <div className={s.inner}>
      <span>Make with &#128151;️</span>
      <a href="#" className={s.link}>
        Our teams
      </a>
    </div>
  </footer>
);

export default Footer;
