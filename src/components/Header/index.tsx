import React from 'react';
import cn from 'classnames';
import { A, usePath } from 'hookrouter';

import { ReactComponent as PokemonLogoSVG } from '../../assets/Logo.svg';
import s from './Header.module.scss';
import { GENERAL_MENU } from '../../routes';

const Header = () => {
  const path = usePath();

  return (
    <header className={s.root}>
      <div className={s.wrap}>
        <div className={s.pokemonLogo}>
          <PokemonLogoSVG />
        </div>
        <div className={s.menuWrap}>
          {GENERAL_MENU.map(({ link, title }) => (
            <A key={title} href={link} className={cn(s.menuLink, { [s.activeLink]: link === path })}>
              {title}
            </A>
          ))}
        </div>
      </div>
    </header>
  );
};

export default React.memo(Header);
