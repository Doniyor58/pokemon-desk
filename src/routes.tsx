import React, { PropsWithChildren } from 'react';

import HomePage from './pages/Home';
import EmptyPage from './pages/Empty';
import PokedexPage from './pages/Pokedex';
import Pokemon, { PokemonProps } from './pages/Pokemon';

export enum LinksEnum {
  HOME = '/',
  POKEDEX = '/pokedex',
  LEGENDARIES = '/legendaries',
  DOCUMENTATION = '/documentation',
  POKEMON = '/pokemon/:id',
}

interface IGeneralMenu {
  title: string;
  link: LinksEnum;
  component: (props: PropsWithChildren<any>) => JSX.Element;
}

export const GENERAL_MENU: IGeneralMenu[] = [
  {
    link: LinksEnum.HOME,
    title: 'Home',
    component: () => <HomePage />,
  },
  {
    link: LinksEnum.POKEDEX,
    title: 'Pokédex',
    component: () => <PokedexPage />,
  },
  {
    link: LinksEnum.LEGENDARIES,
    title: 'Legendaries',
    component: () => <EmptyPage title="Legendaries" />,
  },
  {
    link: LinksEnum.DOCUMENTATION,
    title: 'Documentation',
    component: () => <EmptyPage title="Documentation" />,
  },
  {
    link: LinksEnum.POKEMON,
    title: 'Pokemon',
    component: ({ id }) => <Pokemon id={id} />,
  },
];

const SECOND_MENU: IGeneralMenu[] = [
  {
    link: LinksEnum.POKEMON,
    title: 'Pokemon',
    component: ({ id }: PokemonProps) => <Pokemon id={id} />,
  },
];

interface IAccMenu {
  [n: string]: (props: PropsWithChildren<any>) => JSX.Element;
}

const routes = [...GENERAL_MENU, ...SECOND_MENU].reduce((acc: IAccMenu, item: IGeneralMenu) => {
  acc[item.link] = item.component;
  return acc;
}, {});

export default routes;
